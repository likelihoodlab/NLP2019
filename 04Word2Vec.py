from gensim.models import word2vec
import logging
import re
import pandas as pd
import jieba.posseg as jp, jieba
import time
from collections import defaultdict
texts = []
dates_lst = []
text = ""
date = list(pd.read_excel(r'D\MACRO\macro'+str(1)+'.0.xls',sheet_name='Sheet').loc[:,'Date'])[0]
dates_lst.append(date)
jieba.load_userdict(r"D\user_dct_macro.txt")
for k in range(1,79):
    df = pd.read_excel(r'D\MACRO\macro'+str(k)+'.0.xls',sheet_name='Sheet')
    dates = list(df.loc[:,'Date'])
    for n in range(len(dates)):
        date = dates[n]
        if type(date) is not str:
            continue
        want = 0
        for word in jp.cut(df.loc[n,'Title'].replace(" ","")):
            if word.flag not in ['eng', 'm' , 'x']:
                want = 1
                break
        if want == 0:
            continue
        #if dates[n] == date:
        text = df.loc[n,'Content']
        #else:
        texts.append(text)
        dates_lst.append(date)
        #text = "" 
print("Loading data finished!")
flags = ('n', 'nr', 'ns', 'nt', 'v', 'd', 'eng')  # 词性
#stopwords = open(r"E:\abc\chinese_stopword1.txt",'r',encoding = 'utf-8').readlines()
print(len(texts))
print(len(dates_lst))
words_ls = []
start_time = time.time()
texts = [re.sub('\\.|-| |\t|_|%|％|\[|\]|/|^|\n|的|个|是|兄|也|了|只','',text) for text in texts]
texts = [re.split('[；;。！？…，,：:、》《~\?\.“”""（）\(\)\t\n]',text) for text in texts]
for text in texts:
    for sentence in text:
        words = [w.word for w in jp.cut(sentence) if w.flag in flags ]
        words_ls.append(words)
    if len(words_ls) % 100 == 0:
        print(len(words_ls))
    #tag += 1
    #print(tag)
end_time = time.time() 
print("Word segment costs " + str(end_time - start_time) + " seconds.")
word_ls_keep = words_ls
frequency = defaultdict(int)
idf = defaultdict(int)
for w in words_ls:
    token_ls = []
    for token in w:
        frequency[token] += 1
        if token in token_ls:
            continue
        else:
            token_ls.append(token)
            idf[token] += 1
#sentences = [[token for token in words if frequency[token] >= 40] for words in words_ls]  
# w2v主程序
logging.basicConfig(format='%(asctime)s:%(levelname)s: %(message)s', level=logging.INFO)  
#sentences =word2vec.Text8Corpus(u"result.txt")  # 加载语料
sentences = words_ls
model = word2vec.Word2Vec(sentences, size=200, min_count = 10)  #训练skip-gram模型，默认window=5
model.save("E:\\abc\\word2vec_all.model")

