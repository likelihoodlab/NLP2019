***
# Sentimental analysis of research reports based on LDA and Word2Vec

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

Introduction
====
......
Papers here:

#### Installation

1. xxxx
2. xxxx
3. xxxx

#### Instructions

1. xxxx
2. xxxx
3. xxxx

Contribution
====
Contributors
-------
- ***Yan Guo***
- ***Ziwei Mei***
- ***Jiawen Zhang***
- ***Kai Zhu***

Set up
====
Python Version
-------
- ***3.6***

Modules needed
-------
- ***logging***
- ***jieba***
- ***gensim***
- ***numpy***
- ***pandas*** 

Contact
====
- zhangjw76@mail2.sysu.edu.cn