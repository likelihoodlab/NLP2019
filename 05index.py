# -*- coding: utf-8 -*-
import numpy as np 
import pandas as pd
import matplotlib.pylab as pl
from gensim.models import word2vec

model = word2vec.Word2Vec.load("D:\\word2vec_all.model")
Ks = [6,20,28]
keywords = ['问题','复苏','衰退','压力','出口','进口','支出','收入','担忧','风险','不确定','危机','急剧','震荡','差','疲','回','落','反弹','上','下','高','低','增','加','减','多','少','涨','跌','宽松','紧缩','紧','松','扩张','升','降','强','弱','乐观','悲观','好','坏','利空']
phi_df = pd.read_excel(r'D:\phi70n.xlsx',sheet_name='Sheet')
K = 70
for k in Ks:
    words_k = list(phi_df["Topic" + str(k)])
    want_k = list(phi_df["Want" + str(k)])
    pos = []
    neg = []
    for t in range(0,len(words_k)):
        if want_k[t] < 0:
            neg.append(words_k[t])
        elif want_k[t] > 0:
            pos.append(words_k[t])
    try: 
        y = model.most_similar(positive = pos, negative = neg, topn = 47046)
    except:
        continue
    y = dict(y)
    for key, value in y.items():
        try:
            want = 0
            for keyword in keywords:
                if keyword in key:
                    want = 1
                    break
            if want == 1:
                phi_df.iloc[words_k.index(key) ,list(phi_df.columns).index("Want" + str(k))] = value
        except:
            pass
weight = []
for k in range(K):
    values = list(phi_df["Want" + str(k)])
    keys = list(phi_df["Topic" + str(k)])
    weight.append(dict(zip(keys, values)))
phi = lda.get_topics() ### get the topic-word matrix 

K = 70
points = []
for d in range(0, len(corpus_month)):
    print(d)
    theta_d = theta_month[d] 
    point_d = [0 for x in range(0,K)]
    mention_d = [0 for x in range(0,K)]
    for tup in corpus_month[d]:
        v = int(tup[0])
        word = vocab[v]
        for k in Ks:
            if word in weight[k]:
                point_d[k] += float(tup[1])*float(weight[k][word])*(phi[k][v]*theta_d[k]/sum([phi[r][v]*theta_d[r] for r in range(K)]))#/length_doc_month[d]
                mention_d[k] += tup[1]*phi[k,v]*theta_d[k]/sum([phi[r][v]*theta_d[r] for r in range(K)])
    for k in range(K):
        if mention_d[k] == 0:
            mention_d[k] = 1
    points.append([point_d[k]/mention_d[k] for k in range(K)])
points_mat_month = np.mat(points)


#################333 Observe the results 
a = np.mean(points_mat_month[:,28])
b = np.std(points_mat_month[:,28])
ind28 = [(float(x)-a)/b for x in points_mat_month[:,28]]
pl.plot(list(range(len(points))), ind28)

a = np.mean(points_mat_month[:,20])
b = np.std(points_mat_month[:,20])
ind20 = [(float(x)-a)/b for x in points_mat_month[:,20]]
pl.plot(list(range(len(points))), ind20)

ind = [float(points_mat_month[x,28] + points_mat_month[x,20]) for x in range(len(corpus_month))]
a = np.mean(t)
b = np.std(t)
ind = [(x-a)/b for x in ind]
pl.plot(list(range(len(points))),  ind)

data = pd.read_excel(r'D:\data.xlsx',sheet_name='Sheet1')
stock = list(data['d_stock'])
pl.scatter(ind[0:-1], stock)
############################# 
fig,ax1 = pl.subplots()
pl.xticks(list(range(len(corpus_month))), months)
ax2 = ax1.twinx()
ax1.plot(range(len(points)-1),ind[0:-1],'--',c='red',label = 't')
ax1.axis([1,151,-0.2,0.1])
ax2.plot(range(len(points)-1),stock,'-',c='blue',label = 't')
ax2.axis([1,151,-1400.,750])
ax1.set_xlabel('Months') #与原始matplotlib设置参数略有不同，使用自己下载的中文宋体，参数位置不可改变
ax1.set_ylabel('Index')
ax2.set_ylabel("D.Stock")
pl.savefig(r'D:\fig\ind_stock.png',bbox_inches = 'tight')
############################# regression
import statsmodels.api as sm
est = sm.OLS(stock, ind[0:-1]).fit()
est.summary()
pl.scatter(ind[0:-1], stock)
pl.xlabel("Index")
pl.ylabel("D.Stock")
pl.savefig(r'D:\fig\scatter.png',bbox_inches = 'tight')


y_fitted = est.fittedvalues
pl.scatter(ind[0:-1], stock)
pl.plot(ind[0:-1], y_fitted, 'r--.',label='OLS')
pl.xlabel("Index")
pl.ylabel("D.Stock")
pl.savefig(r'D:\fig\reg.png',bbox_inches = 'tight')



