***
# Sentimental analysis of research reports based on LDA and Word2Vec

#### Description
{**When you're done, you can delete the content in this README and update the file with details for others getting started with your repository**}

Introduction
====
This project is aiming to solve financial problems with the implementation of specific 2 models： LDA(Latent Dirichlet Allocation) and word2vec, where LDA is a generative probabilistic model for collections of discrete data and word2vec is a tool for word-embedding based on deep learning.
We try to determine the degree to which the research reports can be true to financial market dynamics and whether they are forward-looking or just second-guessers.
Papers here: [English Version](https://gitee.com/likelihoodlab/NLP2019/raw/master/NLP_paper.pdf)

Experiment
====
(not completed) :exclamation: 

Contribution
====
Contributors
-------
- ***Yan Guo***
- ***Ziwei Mei***
- ***Jiawen Zhang***
- ***Kai Zhu***

Acknowledgement
-------
We would like to say thanks to MingWen Liu from ShiningMidas Private Fund for his generous help throughout the research. We are also grateful to Xingyu Fu from Sun Yat-sen University for his guidance and help. With their help, this research has been completed successfully. 

Set up
====
Python Version
-------
- ***3.6***

Modules needed
-------
- ***logging***
- ***jieba***
- ***gensim***
- ***numpy***
- ***pandas*** 
- ***sys*** 
- ***os*** 
- ***re*** 
- ***wordcloud*** 
- ***time*** 
- ***PIL*** 
- ***collections*** 
- ***matplotlib*** 

Contact
====
- guoy56@mail2.sysu.edu.cn
- meizw@mail2.sysu.edu.cn
- zhangjw76@mail2.sysu.edu.cn
- benjaminkz@outlook.com