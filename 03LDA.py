# -*- coding: utf-8 -*-
import numpy as np 
import pandas as pd
from wordcloud import WordCloud 
import PIL.Image as Image
import os 
import time 
from collections import defaultdict 
import matplotlib.pylab as pl
from gensim import corpora, models
import jieba.posseg as jp, jieba
import re 
import math
################## load texts
texts = []
dates_lst = []
text = ""
date = list(pd.read_excel(r'D:\MACRO\macro'+str(1)+'.0.xls',sheet_name='Sheet').loc[:,'Date'])[0]
jieba.load_userdict(r"D:\dict\user_dct_macro.txt")
for k in range(1,79):
    df = pd.read_excel(r'D:\MACRO\macro'+str(k)+'.0.xls',sheet_name='Sheet')
    dates = list(df.loc[:,'Date'])
    for n in range(len(dates)):
        date = dates[n]
        if (type(date) != str and math.isnan(date)) or date[0:4] == str(2006) or date[0:7] == "2019-08":
            continue
        if type(date) is not str:
            continue
        want = 0
        for word in jp.cut(df.loc[n,'Title'].replace(" ","")):
            if word.flag not in ['eng', 'm' , 'x']:
                want = 1
                break
        if want == 0:
            continue
        dates_lst.append(date)
print("Loading data finished!")
################################################ Word Segment 
flags = ('n', 'nr', 'ns', 'nt', 'v', 'd', 'eng')  # 词性
stopwords = open(r"D:\chinese_stopword1.txt",'r',encoding = 'utf-8').readlines()
words_ls = []
start_time = time.time()
print(len(texts))
print(len(dates_lst))
texts = [re.sub('\\.|-| |\t|_|%|％|\[|\]|/|^|\n|的|个|是|兄|也|了|只','',text) for text in texts]
texts = [re.split('[；;。！？…，,：:、》《~\?\.“”""（）\(\)\t\n]',text) for text in texts]
for text in texts:
    words = []
    for sentence in text:
        words += [w.word for w in jp.cut(sentence) if w.flag in flags and w.word not in stopwords and len(w.word) >= 2]
    words_ls.append(words)
    if len(words_ls) % 100 == 0:
        print(len(words_ls))
    #tag += 1
    #print(tag)
end_time = time.time()	
print("Word segment costs " + str(end_time - start_time) + " seconds.")
word_ls_keep = words_ls
###############################3 Keep effective words 
frequency = defaultdict(int)
idf = defaultdict(int)
for w in words_ls:
    token_ls = []
    for token in w:
        frequency[token] += 1
        if token in token_ls:
            continue
        else:
            token_ls.append(token)
            idf[token] += 1
words_ls = [[token for token in words if frequency[token] >= 40] for words in words_ls]   
###########################################################################3
dictionary = corpora.Dictionary(words_ls)
corpus = [dictionary.doc2bow(words) for words in words_ls]
print("The vocaburary contains " + str(len(dictionary)) + " words.")
corpus_dict = [dict(x) for x in corpus]
length_doc = []
length = 0
for dct in corpus_dict:
    for value in dct.values():
        length += value
    length_doc.append(length)
    length = 0 
length_corpus = sum(length_doc)  
############ Write word segment results 
words_ls_write = [','.join(x)+'\n' for x in words_ls]
fo = open(r"D:\word_ls_macro_all_short.txt", "w")          ######### Final phrase dictionary 
fo.writelines(words_ls_write)
fo.close()

word_ls = open(r"D:\word_ls_macro_all_short.txt").readlines()
word_ls = [re.sub('\n', '', words) for words in word_ls]
words_ls = [re.split(',',words) for words in word_ls]
##########################################################################33
'''Generate dictionart and corpus'''
dictionary = corpora.Dictionary(words_ls)
corpus = [dictionary.doc2bow(words) for words in words_ls]
print("The vocaburary contains " + str(len(dictionary)) + " words.")
####################################################3
############### How many phrases in total? 
corpus_dict = [dict(x) for x in corpus]
length_doc = []
length = 0
for dct in corpus_dict:
    for value in dct.values():
        length += value
    length_doc.append(length)
    length = 0
    
length_corpus = sum(length_doc)
#########################################################
'''LDA model training'''
phis = []
thetas = []
perps = []
ldas = []
Ks = [10*K for K in range(1,11)]
for K in Ks: 
#K = 20  ##### set number of topics 
    start_time = time.time()
    lda = models.ldamodel.LdaModel(corpus=corpus, id2word=dictionary, num_topics=K, iterations = 5000)
    ldas.append(lda)
    end_time = time.time()
    print("Model of " + str(K) + " topics costs " + str(end_time-start_time) + " seconds!")
    if K == 10:
        id_token = dictionary.id2token
        #print(id_token)
        vocab = []
        for word in id_token.values():
        	vocab.append(word)
    phi = lda.get_topics()
    phis.append(phi)
###########################################################

###################### Generate necessary variables for the following parts 
id_token = dictionary.id2token
topic_term = lda.get_topics()
#print(id_token)
vocab = []
for word in id_token.values():
	vocab.append(word)
#print(vocab)
n = 0
for lda in ldas:
    lda.save("D:\\model_lda\\lda" + str(n) + ".model")
    n += 1
################################################################ Generate phi matrix and WordClouds 
lda = ldas[6]
K = 70
topic_number = []
for k in range(0,K):
    topic_number += ["Topic" + str(k), "Prob" + str(k), "Want" + str(k)]
phi = pd.DataFrame(columns = topic_number)
cur_path = 'D:\\'
mask_pic = np.array(Image.open(os.path.join(cur_path, 'mask.png'))) # for reshaping the wordclouds
for k in range(0,K):
    dct = dict(lda.get_topic_terms(topicid = k, topn=len(dictionary)))
    dct = sorted(dct.items(), key = lambda item: item[1], reverse = 1)
    probs = [dct[n][1] for n in range(0,500)]
    words = [vocab[dct[n][0]] for n in range(0,500)]
    phi["Prob" + str(k)] = probs
    phi["Topic" + str(k)] = words
    txt = " ".join([words[n] for n in range(len(probs)) for i in range(int(1e6*probs[n]))])
    wordcloud = WordCloud(font_path = 'D:\simhei.ttf', # font 
					background_color = 'white', # Background Color 
                    max_words = 100, # Max number of words 
                    max_font_size = 75, # Largest size 
                    collocations = False,
                    mask = mask_pic # Reshape the wordclouds using a picture file 
                    ).generate(txt)
    path = r'D:\WordClouds\70Topic '+str(k)+'.jpg'  #### Save pictures of wordcluods (change the path)#####################
    wordcloud.to_file(path)
phi.to_excel(r'D:\phi70n.xlsx',sheet_name='Sheet') #### write the matrix into an excel file 
##############################################################################################
'''Regard texts in the same month as a document and estimate the topic distribution'''
words_lst_month = []
words_month = []
date_basis = dates_lst[0]
num = 0
length_doc_month = []
month_ls = []
for date in dates_lst:
    if date[0:7] in date_basis:
        words_month += words_ls[num]
        #l += length_doc[num]
    else:
        words_lst_month.append(words_month)
        length_doc_month.append(len(words_month))
        month_ls.append(date_basis[0:7])
        words_month = words_ls[num]
        date_basis = date
       
    num += 1
    if num == len(dates_lst):
        words_lst_month.append(words_month)
        length_doc_month.append(len(words_month))
        month_ls.append(date_basis[0:7])
words_lst_month = list(reversed(words_lst_month))
length_doc_month = list(reversed(length_doc_month))

dictionary_month = corpora.Dictionary(words_lst_month)
'''基于词典，使【词】→【稀疏向量】，并将向量放入列表，形成【稀疏向量集】'''
corpus_month= [dictionary_month.doc2bow(words) for words in words_lst_month] 

theta_month, _ = lda.inference(corpus_month)
theta_month /= theta_month.sum(axis=1)[:, None]
##############################3
'''Show specific topic distribution'''
d = pd.date_range(start = '2007-01-31', periods = 151, freq = '1M')
months = [str(x)[2:4]+str(x)[5:7] for x in d]
for num in range(len(months)):
    if num % 24 != 0:
        months[num] = ''
pl.plot(list(range(len(corpus_month))),  theta_month[:,33])
pl.xticks(list(range(len(corpus_month))), months)
pl.xlabel("Months")
pl.ylabel("Topic 33")
pl.savefig(r'D:\fig\topic33.png',bbox_inches = 'tight')

pl.plot(list(range(len(corpus_month))),  theta_month[:,64])
pl.xticks(list(range(len(corpus_month))), months)
pl.xlabel("Months")
pl.ylabel("Topic 64")
pl.savefig(r'D:\fig\topic64.png',bbox_inches = 'tight')



